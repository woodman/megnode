import model
import os
import pickle
import time
import numpy as np
import sys
import argparse
import tqdm
import matplotlib.pyplot as pl
import jax.numpy as jp

parser = argparse.ArgumentParser(description='Run a node model.')
parser.add_argument('-c', '--checkpoint', help='checkpoint file name')
args = parser.parse_args()

# load
with open(args.checkpoint, 'rb') as fd:
    pkl = pickle.load(fd)

# ['params', 'optimizer', 'epoch', 'avg_l', 'nans', 'config']
print(pkl.keys())

# config
# TODO to argparser (or dataclass -> argparse?)


class config:
    pass


for key, val in pkl['config'].items():
    print(f'config.{key} = {val}')
    setattr(config, key, val)

# model
_, run, vgloss = model.make_model(config)
wb = pkl['params']

# load data for run
path = '/work/duke/hcp-ready'
x1 = np.load(f'{path}/megmodes-100307.npy')[20*1024:, :config.num_modes] / 1e-5
# megmodes-293748.npy
x2 = np.load(f'{path}/megmodes-293748.npy')[20*1024:, :config.num_modes] / 1e-5

C, M, B = config.ctx_len, config.num_modes, config.batch_size
F = config.pred_len
nw = 5

# plot predictions in train and test
for i, (x_, label_) in enumerate(zip((x1, x2), ('train', 'test'))):
    pl.subplot(1, 2, i+1)

    x = np.array([x_[i:C+F*nw+i] for i in range(B)]).transpose((1, 2, 0))
    y = x[:C]
    for i in range(nw):
        y = np.concatenate([y, run(wb, y[-C:])], axis=0)

    t = np.r_[:C+F*nw]/678.0
    s = 50.0
    pl.plot(t, y[..., 0] + np.r_[:M]*s, 'r', alpha=0.4)
    pl.plot(t, x[..., 0] + np.r_[:M]*s, 'k', alpha=0.4)
    pl.axvline(C/678.0)
    pl.axvline((C+F)/678.0)
    pl.title(label_)

pl.show()
