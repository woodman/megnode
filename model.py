from jax.example_libraries.optimizers import adam, clip_grads
import jax
import jax.numpy as jp
import vbjax as vb


def make_model(config):
    L = config.num_latent + 1
    in_dim = config.ctx_len * (config.num_modes * L + 1)

    # TODO convert to flax for dropout
    # TODO allow for low rank in first N layers
    # TODO maybe a conv1d for the first layer

    wb, wbf = vb.make_dense_layers(
        in_dim,
        latent_dims=config.arch,
        out_dim=config.num_modes*L,
        init_scl=config.init_scale)

    # positional encoding
    dt = 0.1
    pe = jp.r_[-config.ctx_len:config.pred_len]*dt
    pe = pe.reshape(-1, 1, 1)

    # TODO hardcoding batch size here means taht for inference
    #      we need to fiddle with the config.batch_size.
    pe = jp.tile(pe, (1, 1, config.batch_size))

    def run(wb, xj):
        C, M, B = xj.shape
        F = config.pred_len
        buf = jp.concatenate([xj, jp.zeros((F, M, B))])  # (C+F, M, B)

        # put the positional encoding in the buffer
        buf = jp.concatenate([pe, buf], axis=1)

        # add the latent state
        if config.num_latent:
            pad = jp.zeros((C+F, M*(L-1), B))
            buf = jp.concatenate([buf, pad], axis=1)
            assert buf.shape == (C+F, M*L+1, B)

        def f(wb, ctx): return wbf(wb, ctx.reshape(C*(1 + M*L), B))

        # TODO use Heun's method
        # TODO do gradient by hand for constant memory usage
        def op(buf, t):
            ctx = jax.lax.dynamic_slice(buf, (t, 0, 0), (C, M*L+1, B))
            xt = ctx[-1, 1:]  # don't include the positional encoding
            # TODO consider "local" neural ODE dynamics
            # XXX implementing aux vars for the neural ODE
            nx = xt + dt*(-xt + f(wb, ctx))
            buf = jax.lax.dynamic_update_slice(buf, nx[None], (t+C, 0, 0))
            return buf, 0
        buf, _ = jax.lax.scan(op, buf, jp.r_[:F])
        return buf[-F:, 1:M+1]  # drop pos enc, latent state

    def loss(wb, x, max_t):
        yh = run(wb, x[:config.ctx_len])
        se = jp.square(yh - x[-config.pred_len:])
        return se[:max_t].mean()

    vgloss = jax.jit(
        jax.value_and_grad(loss),
        static_argnames=['max_t'])

    return wb, run, vgloss


def make_model_online(config):
    "Uncommented copy of above with loss computed online."
    
    L = config.num_latent + 1
    in_dim = config.ctx_len * (config.num_modes * L + 1)
    wb, wbf = vb.make_dense_layers(
        in_dim,
        latent_dims=config.arch,
        out_dim=config.num_modes*L,
        init_scl=config.init_scale)
    
    dt = 0.1
    pe = jp.r_[-config.ctx_len:0.0]*dt
    print('pe.shape', pe.shape)
    pe = pe.reshape(-1, 1, 1)
    pe = jp.tile(pe, (1, 1, config.batch_size))

    def loss(wb, xj):
        CF, M, B = xj.shape
        F = CF - config.ctx_len
        C = config.ctx_len

        pad = jp.zeros((C, M*config.num_latent, B))
        buf = jp.concatenate([pe, xj[:C], pad], axis=1)
        assert buf.shape == (C, M*L+1, B)

        def f(wb, ctx): return wbf(wb, ctx.reshape(C*(1 + M*L), B))

        def op(buf, nx_obs):
            t = buf[-1, 0]
            xt = buf[-1, 1:]
            nx = xt + dt*(-xt + f(wb, buf))
            # compute loss
            loss = jp.square(nx[1:M+1] - nx_obs)
            # update buf
            buf = jp.roll(buf, shift=-1, axis=0)
            buf = buf.at[-1,1:].set(nx)
            buf = buf.at[-1, 0].set(t+dt)
            return buf, loss

        _, sse = jax.lax.scan(op, buf, xj[C:])
        return jp.mean(sse)

    jit_lossgrad = jax.jit(jax.value_and_grad(loss))
    return wb, jit_lossgrad


# TODO first dense layer largest, consider low rank or 1.5bit there
# TODO time scale separations: early training is slowest time scales
# TODO uncertainty e.g. bayesian SDE
# TODO uncertainty e.g. 95% CI intervals (q0, q1, p in interval), median for recurrent fwd?
# TODO quantization of data and/or weights, e.g 1.5bit
# TODO manual derivatives for better memory usage
