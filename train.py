import os
import pickle
import time
import numpy as np
import sys
import argparse
import tqdm

parser = argparse.ArgumentParser(description='Train a node model.')
parser.add_argument('-B', '--batch_size', type=int, default=128, help='Batch size')
parser.add_argument('-R', '--learning_rate', type=float, default=5e-4, help='Learning rate')
parser.add_argument('-A', '--arch', type=str, default='(4096,2048,1024,512)', help='Architecture')
parser.add_argument('-G', '--no_gpu', default=False, action='store_true', help='No GPU')
parser.add_argument('-E', '--num_epochs', type=int, default=1000, help='Number of epochs')
parser.add_argument('-W', '--wandb', default=False, action='store_true', help='Use wandb')
parser.add_argument('-C', '--ctx-len', type=int, default=128, help='Context length')
parser.add_argument('--pred-len', type=int, default=32, help='Prediction length')
parser.add_argument('-P', '--progress', default=False, action='store_true', help='Show progress bar')
parser.add_argument('--single-core', default=False, action='store_true', help='Force single core use only')
parser.add_argument('--max-files', type=int, default=1, help='Max files to use')
parser.add_argument('--use-memmap', default=False, action='store_true', help='Use memmap')
parser.add_argument('--profiler', default=False, action='store_true', help='Run profiler')
parser.add_argument('--slug', default='', help='extra checkpoint name')
parser.add_argument('--startpoint', help='checkpoint from which to load weights')
parser.add_argument('--online', action='store_true', default=False)
args = parser.parse_args()

# config
# TODO to argparser (or dataclass -> argparse?)
class config:
    # TODO get from os.environ
    path = '/work/duke/hcp-ready'
    # path = '/Users/duke/Downloads/megmodes/'
    slug = args.slug
    use_memmap = args.use_memmap
    max_files = args.max_files
    np_seed = 42
    batch_size = args.batch_size
    split = 0.8
    overlap = 5 # 7ms
    overlap = args.pred_len # allow larger numbers of subjects
    ctx_len = args.ctx_len

    # TODO progressively increase pred_len..
    pred_len = args.pred_len

    # TODO how could we progressively increase num_modes?
    num_modes = 32
    no_gpu = args.no_gpu
    learning_rate = args.learning_rate

    # TODO test similar parameter count, walltime, flop or Wh budget
    #arch = 1024,512,256,128
    arch = eval(args.arch)
    num_latent = 1
    init_scale = 1e-6
    num_epochs = args.num_epochs
    grad_clip = 10.0
    single_core = args.single_core

    checkpoint = True
    startpoint = args.startpoint
    checkpoint_path = '/work/duke/checkpoints'

    online = args.online

# checkpoint maybe
if args.startpoint:
    with open(args.startpoint, 'rb') as fd:
        startpoint = pickle.load(fd)
    for key, val in startpoint['config'].items():
        if getattr(config, key) != val:
            print('checkpoint says', key, val,
                  'but args/config say', getattr(config, key))

# global initialization
np.random.seed(config.np_seed)
if config.no_gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = ''
    if config.single_core:
        os.environ['XLA_FLAGS'] = '--xla_force_host_platform_device_count=1 --xla_cpu_multi_thread_eigen=false intra_op_parallelism_threads=1'

# data
import data
xs, win_per, win_idx = data.read_files(config)
train_ix, test_ix = data.make_split(config, win_idx)

# model
import model
import jax.numpy as jp
wb, run, vgloss = model.make_model(config)
_, vgloss_online = model.make_model_online(config)
nparams = 0
for _ in wb:
    for __ in _:
        nparams += __.size
print('nparams', nparams)

if args.startpoint:
    print('loading weights from checkpoint')
    wb = startpoint['params']

# check models against one another
def _verify_online():
    max_t = 5
    b = jp.array(data.make_batch(config, win_idx, train_ix[0], xs))
    l1, g1 = vgloss(wb, b, max_t)
    l2, g2 = vgloss_online(wb, b[:config.ctx_len+max_t])
    print('batch vs online loss', l1, l2)

#_verify_online()

# optimizer
lr = config.learning_rate / 512 * config.batch_size
oinit, oup, oget = model.adam(lr)
o = oinit(wb)

# max t schedule
max_t_inc =  (len(train_ix)/2) / config.pred_len

# start log
config_ = {k:getattr(config, k) for k in dir(config) if not k.startswith('_')}
print(config_)
if args.wandb:
    import wandb
    wandb.init(project='megnode', config=config_)


# epoch loop
print(f'epoch of {len(train_ix)} train and {len(test_ix)} val batch lengths')
for epoch in range(config.num_epochs):

    train_ix, test_ix = data.make_split(config, win_idx)
    b = jp.array(data.make_batch(config, win_idx, train_ix[0], xs))

    avg_l = 0
    nans = 0
    et0 = time.time()

    seq = train_ix[1:]
    if args.progress:
        seq = tqdm.tqdm(seq, ncols=80)

    for i, ix in enumerate(seq):
        # max t schedule stabilizes forecast training
        max_t = min(config.pred_len, int(i / max_t_inc) + 1)
        # compute loss
        if config.online:
            lg = vgloss_online(oget(o), b[:config.ctx_len+max_t])
        else:
            lg = vgloss(oget(o), b, max_t)
        # load next batch while vgloss runs async
        b = jp.array(data.make_batch(config, win_idx, ix, xs))
        # unpack
        l, g = lg

        # skipping nans has not really helped recover in the past
        # perhaps just stop the run if it fails?
        if not np.isfinite(l) or l>1e3:
            nans += 1
            continue

        avg_l += l
        if config.grad_clip:
            g = model.clip_grads(g, config.grad_clip)
        o = oup(i,g,o)
        wb = oget(o)

        if args.progress:
            seq.set_description(f'l {avg_l/(i+1):0.1f}')

    et1 = (time.time() - et0)
    
    avg_val_l = 0
    val_nans = 0
    seq = test_ix
    if args.progress:
        seq = tqdm.tqdm(seq, ncols=80)
    for i, ix in enumerate(seq):
        b = jp.array(data.make_batch(config, win_idx, ix, xs))
        # XXX val_loss for full pred len, not max_t != train loss
        if config.online:
            l, _ = vgloss_online(oget(o), b)
        else:
            l, _ = vgloss(oget(o), b, config.pred_len)
        if not np.isfinite(l) or l>1e3:
            val_nans += 1
        else:
            avg_val_l += l

    print(f'e{epoch} i{i}/{len(train_ix)} t{max_t} l{avg_l/len(train_ix):0.3g} n{nans} e{et1:0.1f}s val l{avg_val_l/len(test_ix):0.3g} n{val_nans}')

    if args.wandb:
        wandb.log(dict(
            epoch=epoch,
            loss=avg_l/len(train_ix),
            val_loss=avg_val_l/len(test_ix),
            nans=nans,
            val_nans=val_nans,
            et=et1,
            ))
    
    if config.checkpoint:
        dash_slug = f'-{config.slug}' if config.slug else ''
        with open(f'{config.checkpoint_path}/megnode-v1{dash_slug}-{epoch}.pkl', 'wb') as fd:
            checkpoint = {
                'params': wb,
                'optimizer': o,
                'epoch': epoch,
                'avg_l': avg_l,
                'nans': nans,
                'config': config_,
            }
            pickle.dump(checkpoint, fd)
