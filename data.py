import numpy as np
import tqdm
import glob
import joblib


def read_files(config):
    xs = []
    files = glob.glob(f'{config.path}/*.npy')
    if config.max_files:
        files = files[:config.max_files]
    for fname in tqdm.tqdm(files):
        x = np.lib.format.open_memmap(fname)
        xs.append(x if config.use_memmap else x[:,:config.num_modes].copy())
    # compute windows per file & total
    win_len = config.ctx_len + config.pred_len
    win_per = np.array([(_.shape[0] - win_len)//config.overlap for _ in xs])
    *_, win_total = win_index_file_offset = np.r_[0, np.cumsum(win_per)]
    return xs, win_per, win_index_file_offset

# shuffle indices of windows for building batches
def _make_win_indices(win_idx, batch_size):
    idx = np.r_[:win_idx[-1]//batch_size*batch_size]
    np.random.shuffle(idx)
    return idx.reshape((-1, batch_size))

def make_split(config, win_idx):
    # need to maintain the split over epochs
    B = config.batch_size
    idx = np.r_[:win_idx[-1]//B*B]
    # train test split
    n_train = int(idx.size * config.split)//B*B
    train_ix = idx[:n_train].reshape((-1, B))
    test_ix = idx[n_train:].reshape((-1, B))
    # train and test don't overlap in time
    # now, shuffle the windows
    np.random.shuffle(train_ix)
    np.random.shuffle(test_ix)
    return train_ix, test_ix

def make_batch(config, win_idx, ix, xs):
    win_len = config.ctx_len + config.pred_len
    @joblib.delayed
    def _load_batch_element(i):
        i_np, = np.argwhere(i >= win_idx)[-1]
        i -= win_idx[i_np]
        x_i = xs[i_np][i*config.overlap:][:win_len]/1e-5
        if config.use_memmap:
            x_i = x_i[:,:config.num_modes]
        assert x_i.shape == (win_len, config.num_modes)
        return x_i
    batch = joblib.Parallel()(_load_batch_element(i) for i in ix)
    #batch = [_load_batch_element(i) for i in ix]
    batch = np.array(batch) # (B, C+F, M)
    batch = batch.transpose((1, 2, 0))
    return batch

